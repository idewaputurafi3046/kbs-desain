@extends('dashboard.main')

@section('subjudul')

<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tabel Aplikasi dan OPD</span>

@endsection

@section('content')
<div class="d-flex flex-column bd-highlight">
  <div class="p-0 bd-highlight">
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('dashboard') }}'" >
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar ">
          <i class="bi bi-speedometer2 pe-2 font-sidebar "></i>
          Dashboard
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('datawebsite') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-clipboard-data pe-2 font-sidebar"></i>
          Database Website
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav"  onclick="location.href='{{ route('tdw') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Database Website
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav active" aria-current="true" onclick="location.href='{{ route('aplikasi') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight">
          <i class="bi bi-table pe-2"></i>
          Tabel Aplikasi dan OPD
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right bi-aktif"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Master Aplikasi
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-grid-1x2-fill pe-2 font-sidebar"></i>
          Embed Aplikasi
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Aplikasi Embed
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-gear-fill pe-2 font-sidebar"></i>
          Setting
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-box-arrow-left pe-2 font-sidebar"></i>
          Keluar
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
  </div>

</div>

@endsection