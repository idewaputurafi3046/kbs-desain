@extends('dashboard.main')

@section('content')

<div class="d-flex flex-column bd-highlight">
    <div class="p-0 bd-highlight">
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav active" aria-current="true" onclick="location.href='{{ route('dashboard') }}'" >
        <div class="d-flex bd-highlight">
          <div class="p-2 w-100 bd-highlight">
            <i class="bi bi-speedometer2 pe-2"></i>
            Dashboard
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right bi-aktif"></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('datawebsite') }}'">
        <div class="d-flex bd-highlight">
          <div class="p-2 w-100 bd-highlight font-sidebar">
            <i class="bi bi-clipboard-data pe-2 font-sidebar"></i>
            Database Website
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right"></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav " onclick="location.href='{{ route('tdw') }}'" >
        <div class="d-flex bd-highlight">
          <div class="p-2 ms-4 w-100 bd-highlight submenu">
            <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
            Tambah Database Website
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right"></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('aplikasi') }}'">
        <div class="d-flex bd-highlight">
          <div class="p-2 w-100 bd-highlight font-sidebar">
            <i class="bi bi-table pe-2 font-sidebar"></i>
            Tabel Aplikasi dan OPD
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right"></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
        <div class="d-flex bd-highlight">
          <div class="p-2 ms-4 w-100 bd-highlight submenu">
            <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
            Tambah Master Aplikasi
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right "></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
        <div class="d-flex bd-highlight">
          <div class="p-2 w-100 bd-highlight font-sidebar">
            <i class="bi bi-grid-1x2-fill pe-2 font-sidebar"></i>
            Embed Aplikasi
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right"></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
        <div class="d-flex bd-highlight">
          <div class="p-2 ms-4 w-100 bd-highlight submenu">
            <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
            Tambah Aplikasi Embed
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right "></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
        <div class="d-flex bd-highlight">
          <div class="p-2 w-100 bd-highlight font-sidebar">
            <i class="bi bi-gear-fill pe-2 font-sidebar"></i>
            Setting
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right"></i>
          </div>
        </div>
      </button>
      <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
        <div class="d-flex bd-highlight">
          <div class="p-2 w-100 bd-highlight font-sidebar">
            <i class="bi bi-box-arrow-left pe-2 font-sidebar"></i>
            Keluar
          </div>
          <div class="p-2 flex-shrink-1 bd-highlight">
            <i class="bi bi-chevron-right"></i>
          </div>
        </div>
      </button>
    </div>
  
  </div>

@endsection

@section('subjudul')
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Statistik Pengakses Website</span>
@endsection

@section('datepicker')
<div class="datepicker">
  <input type="date" name="dateofbirth" class="startdate">
  <span style="color:black;font-weight:100" class="px-3">To</span>
  <input type="date" name="dateofbirth" class="enddate">
</div>

@endsection

@section('content2')
<p id="statistik">Statistik User</p>

@endsection