@extends('dashboard.main')

@section('subjudul')

<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tabel Database Website</span>

@endsection

@section('content')
<div class="d-flex flex-column bd-highlight">
  <div class="p-0 bd-highlight">
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('dashboard') }}'" >
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar ">
          <i class="bi bi-speedometer2 pe-2 font-sidebar "></i>
          Dashboard
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav active " aria-current="true" onclick="location.href='{{ route('datawebsite') }}'" >
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight">
          <i class="bi bi-clipboard-data pe-2"></i>
          Database Website
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right bi-aktif "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav " onclick="location.href='{{ route('tdw') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Database Website
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('aplikasi') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-table pe-2 font-sidebar"></i>
          Tabel Aplikasi dan OPD
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Master Aplikasi
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-grid-1x2-fill pe-2 font-sidebar"></i>
          Embed Aplikasi
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Aplikasi Embed
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-gear-fill pe-2 font-sidebar"></i>
          Setting
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-box-arrow-left pe-2 font-sidebar"></i>
          Keluar
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
  </div>
</div>
@endsection

@section('content2')

<div>
  <form class="searchtbl">
    <button type="submit" class="search-button"><i class="bi bi-search icon-search2"></i></button>
    <input class="search-container" type="search" placeholder="Telurusi website ini" aria-label="Search">
    <button type="button" class="tambahdata" onclick="location.href='{{ route('tdw') }}'">
      <p>+ Data Website</p>
    </button>
  </form>
</div>

<div class="bdr table">
  <table class="table-hover table-responsive">
    <thead>
      <tr class="thead">
        <th class="th">No.</th>
        <th class="th2">Judul</th>
        <th>Pemilik Aplikasi</th>
        <th>Tindakan</th>
      </tr>
    </thead>
    <tr>
      <td>01.</td>
      <td style="text-align: left">Syarat Pembuatan KTP - Website Portal Resmi Pemerintah Kota Denpasar</td>
      <td>Disdukcapil</td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr>
    <tr>
      <td>02.</td>
      <td style="text-align: left">Layanan - Dinas Kependudukan dan Pencatatan Sipil</td>
      <td>Disdukcapil</td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr>
    <tr>
      <td>03.</td>
      <td style="text-align: left">Dinas DukCapil - Pemerintah Kabupaten Jembrana</td>
      <td>Disdukcapil</td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr>
    <tr>
      <td>04.</td>
      <td style="text-align: left">Pelayanan Kartu Tanda Penduduk - Website Resmi Dinas Kabupaten Badung</td>
      <td>Disdukcapil</td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr>
    
  </table>
</div>


@endsection