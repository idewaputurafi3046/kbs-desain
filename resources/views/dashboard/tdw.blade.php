@extends('dashboard.main')

@section('subjudul')

<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tambah Database Website</span>

@endsection

@section('content')
<div class="d-flex flex-column bd-highlight">
  <div class="p-0 bd-highlight">
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('dashboard') }}'" >
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar ">
          <i class="bi bi-speedometer2 pe-2 font-sidebar "></i>
          Dashboard
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('datawebsite') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-clipboard-data pe-2 font-sidebar"></i>
          Database Website
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav active" aria-current="true" onclick="location.href='{{ route('tdw') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu-aktif">
          <i class="bi bi-plus-circle-fill pe-2 submenu-aktif"></i>
          Tambah Database Website
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right bi-aktif"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav" onclick="location.href='{{ route('aplikasi') }}'">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-table pe-2 font-sidebar"></i>
          Tabel Aplikasi dan OPD
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Master Aplikasi
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-grid-1x2-fill pe-2 font-sidebar"></i>
          Embed Aplikasi
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 ms-4 w-100 bd-highlight submenu">
          <i class="bi bi-plus-circle-fill pe-2 submenu"></i>
          Tambah Aplikasi Embed
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right "></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-gear-fill pe-2 font-sidebar"></i>
          Setting
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
    <button type="button" class="px-3 list-group-item list-group-item-action btn-nav">
      <div class="d-flex bd-highlight">
        <div class="p-2 w-100 bd-highlight font-sidebar">
          <i class="bi bi-box-arrow-left pe-2 font-sidebar"></i>
          Keluar
        </div>
        <div class="p-2 flex-shrink-1 bd-highlight">
          <i class="bi bi-chevron-right"></i>
        </div>
      </div>
    </button>
  </div>

</div>

@endsection

@section('content2')
          
              <form class="form">
                <div class="row mb-3">
                  <label for="inputEmail3" class="col-sm-2 col-form-label text-dark judul">Nama Laporan :</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input" placeholder="Nama Laporan">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputPassword3" class="col-sm-2 col-form-label text-dark judul">Grup Laporan :</label>
                  <div class="col-sm-10 dropdown">
                    <select name="fms" id="FMS">
                      <option value=""></option>
                      <option value="FMS">FMS</option>
                      <option value="URL">URL</option>
                    </select>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputPassword3" class="col-sm-2 col-form-label text-dark judul">Master Aplikasi :</label>
                  <div class="col-sm-10 dropdown">
                    <select name="fms" id="FMS">
                      <option value=""></option>
                      <option value="simpeg">SIMPEG</option>
                      <option value="bali1data">Bali Satu Data</option>
                    </select>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputEmail3" class="col-sm-2 col-form-label text-dark judul">Pemilik Aplikasi :</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input" disabled placeholder="Pemilik Aplikasi">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputEmail3" class="col-sm-2 col-form-label text-dark judul">Kode Laporan :</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input" placeholder="Kode Laporan">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="inputPassword3" class="col-sm-2 col-form-label text-dark judul">Sumber Data :</label>
                  <div class="col-sm-10 dropdown">
                    <select name="fms" id="FMS">
                      <option value=""></option>
                      <option value="sumber1">Sumber 1</option>
                      <option value="sumber2">Sumber 2</option>
                    </select>
                  </div>
                </div>
                <div class="button">
                  <button type="submit" class="btn btn-primary">Batal</button>
                  <button type="button" class="btn btn-primary" id="simpan" onclick="location.href='{{ route('datawebsite') }}'">Simpan</button>
                </div>
                
              </form>
            </div>
          </div>
@endsection