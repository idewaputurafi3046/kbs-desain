<?php

use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// main kerangka
Route::get('/dashboard', function () {
    return view('dashboard.main');
})->name('main');

//dashboard
Route::get('/', function () {
    return view('dashboard.index');
})->name('dashboard');

//tabel data website
Route::get('/admin/datawebsite', function () {
    return view('dashboard.tabeldatawebsite');
})->name('datawebsite');

// tambah data website
Route::get('/admin/tambahdatawebsite', function () {
    return view('dashboard.tdw');
})->name('tdw');

// aplikasi dan OPD
Route::get('/admin/aplikasiopd', function () {
    return view('dashboard.aplikasi');
})->name('aplikasi');

// Route::get('/admin/main', function () {
//     return view('dashboard.main');
// })->name('main');

// auth

// dashboard
    Route::get('/admin/dashboard', function () {
        return view('dashboard.dashboard');
    });
// landing

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
